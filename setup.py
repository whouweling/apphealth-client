import setuptools


setuptools.setup(
    name="apphealth-whouweling", # Replace with your own username
    version="0.0.1",
    author="Wouter Houweling",
    author_email="info@metricboard.io",
    description="APPHealth client",
    long_description="",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/whouweling/apphealth-client.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
)