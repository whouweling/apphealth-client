
from .tracers.template import TemplateTracer
from .tracers.connection import ConnectionTracer
from .tracers.json import JsonTracer
from .tracers.query import QueryTracer

TRACERS = [
    TemplateTracer,
    JsonTracer,
    QueryTracer,
    ConnectionTracer
]