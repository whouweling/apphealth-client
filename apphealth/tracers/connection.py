import time

from collections import defaultdict

from .base import Tracer


class ConnectionTracer(Tracer):

    name = "request"
    module = "urllib3.connection"
    methods = ["HTTPConnection.connect"]
    alias = {
        "HTTPConnection.urlopen": "request"
    }


    def get_key(self, args, kwargs):

        return args[0].host # Extract the host
