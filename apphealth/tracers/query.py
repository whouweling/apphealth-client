
from .base import Tracer

class QueryTracer(Tracer):

    name = "query"

    module = "django.db.backends.utils"

    methods = ["CursorWrapper.execute"]

    alias = {
        "CursorWrapper.execute": "execute"
    }

    def get_key(self, args, kwargs):

        if len(args) > 2:
            return args[1] # Pre-bound SQL query

        return super(QueryTracer, self).get_key(args, kwargs)
