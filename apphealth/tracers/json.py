import time

from collections import defaultdict

from .base import Tracer


class JsonTracer(Tracer):

    name = "serialize"
    module = "json"
    methods = ["loads", "dumps"]
