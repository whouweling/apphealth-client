import time

from collections import defaultdict

from .base import Tracer


class TemplateTracer(Tracer):

    name = "template"
    module = "django.template"
    methods = ["Template.render"]
    alias = {
        "Template.render": "render"
    }


    def get_key(self, args, kwargs):

        return args[0].name # Extract the template name
