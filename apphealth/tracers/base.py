import time
import threading

from collections import defaultdict


class ThreadLocal(threading.local):

    def __init__(self):
        self.tracing = False
        self.calls = {}


class Tracer(object):

    alias = {}

    def __init__(self):
        self._import = None
        self.wrapped = {}
        self.reset()
        self.installed = False
        self.start = None

    def get_name(self):
        return self.name

    def reset(self):
        self.local = ThreadLocal()

    def flush(self):
        calls = self.local.calls
        self.reset()
        return calls

    def get_key(self, args, kwargs):
        return "<all>"

    def wrap(self, method):

        try:
            cls, target = method.split(".")
            source = getattr(self._import, cls)

        except ValueError:
            target = method
            source = self._import

        func = getattr(source, target)

        name = self.alias.get(method, method)

        self.local.calls[name] = defaultdict(lambda: (0, 0, list()))

        def _wrapper(*args, **kwargs):

            start = time.time() - self.start
            result = func(*args, **kwargs)
            end = time.time() - self.start

            key = self.get_key(args, kwargs)

            if self.local.tracing: # Should we trace this call on this thread?

                (count, duration, calls) = self.local.calls[name][key]

                calls.append((round(start*1000, 4),
                              round(end*1000, 4)))

                if len(calls) > 150:
                    calls = []

                count += 1
                duration += (end - start)

                self.local.calls[name][key] = (count, duration, calls)

            return result

        setattr(source, target, _wrapper)

        self.wrapped[method] = func

    def unwrap(self, method):

        try:
            cls, target = method.split(".")
            source = getattr(self._import, cls)

        except ValueError:
            target = method
            source = self._import

        setattr(source, target, self.wrapped[method])
        del self.wrapped[method]

    def uninstall(self):

        for method in self.methods:
            self.unwrap(method)

        self.installed = False
        self.local.tracing = False

    def install(self):

        self.reset()

        if self.installed:
            return

        fromlist = []
        for method in self.methods:
            try:
                (cls, _) = method.split(".", 1)
                fromlist.append(cls)
            except ValueError:
                fromlist.append(method)

        self._import = __import__(self.module, None, None, fromlist)

        for method in self.methods:
            self.wrap(method)

        self.start = time.time()
        self.installed = True

        self.local.tracing = True

