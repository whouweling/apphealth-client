from django.apps import AppConfig


class PerftraceConfig(AppConfig):
    name = 'perftrace'
