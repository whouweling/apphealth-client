

import resource
import time
import threading

import logging


from django.conf import settings

from apphealth.helpers.collector import CollectorThread

from apphealth.helpers.scoreboard import ScoreBoard
from apphealth.helpers.tracer import Tracer, TracerManager


logger = logging.getLogger(name="apphealth")

_collector_thread = None


class PerfTraceMiddleware:

    def __init__(self, get_response):

        self.get_response = get_response

        self.score_board = ScoreBoard()
        self.tracer_manager = TracerManager()

        self._thread_local = threading.local()

        global _collector_thread
        _collector_thread = CollectorThread(score_board=self.score_board,
                                            tracer_manager=self.tracer_manager)

        _collector_thread.start()

        self.last_collector_run = time.time()

        #self.test = []

        logger.info("logging app health to '{}'".format(settings.APPHEALTH_API_URL))

    def __call__(self, request):

        rusage_start = resource.getrusage(resource.RUSAGE_SELF)[0]

        self._thread_local.view = None

        start_time = time.time()
        response = self.get_response(request)
        end_time = time.time()

        view = self._thread_local.view

        if getattr(self._thread_local, "tracer", None):
            self.tracer_manager.register_trace(view=view,
                                               name=request.method,
                                               trace=self._thread_local.tracer.stop())

        elapsed = (end_time - start_time) * 1000

        r_usage = resource.getrusage(resource.RUSAGE_SELF)[0] - rusage_start
        external = elapsed - r_usage

        self.score_board.register(
            view=view,
            start_time=start_time,
            end_time=end_time,
            duration=elapsed,
            cpu=r_usage,
            wait=external,
            request_method=request.method,
            response_size=0, # len(response.content), # FIXME: not sure if we can call this for all response types
            response_code=response.status_code,
            path=request.path,
            user=request.user.id,
        )

        return response

    def process_view(self, request, view_func, view_args, view_kwargs):

        self._thread_local.view = "%s.%s" % (view_func.__module__,
                                             view_func.__name__)

        self._thread_local.tracer = None

        #print("_perftrace_middleware_trace", self.tracer_manager.need_trace(request._perftrace_middleware_view))
        if self.tracer_manager.need_trace(self._thread_local.view):
            tracer = Tracer()
            if tracer.start():
                self._thread_local.tracer = tracer


