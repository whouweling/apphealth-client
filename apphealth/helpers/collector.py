from datetime import datetime

import logging
import os
import platform
import socket
import threading
import time
import uuid
import random
import django
import requests

from django.conf import settings

from apphealth.helpers.load import get_load
from apphealth.helpers.memory import get_memory_usage


class CollectorThread(threading.Thread):

    def __init__(self, score_board, tracer_manager):

        self.instance = str(uuid.uuid4())

        self.score_board = score_board
        self.tracer_manager = tracer_manager

        self.api_url = getattr(settings, "APPHEALTH_API_URL", "")
        self.project_key = getattr(settings, "APPHEALTH_PROJECT_KEY", "")
        self.collector_interval = getattr(settings, "APPHEALTH_COLLECTOR_INTERVAL", 60)

        self.node_name = socket.gethostname()

        self.first_contact = True
        self.stop_requested = False
        self.last_send = None
        self.last_send_size = None

        super(CollectorThread, self).__init__()

        self.daemon = True


    def run(self):

        # Small random delay to give the app some time to startup
        time.sleep(random.randint(1, 20))

        while True:

            result = self.score_board.flush()

            result.update({
                "instance": self.instance,
                "pid": os.getpid(),
                "node": self.node_name,
                "load": get_load(),
                "memory": get_memory_usage()
            })

            if self.first_contact:
                result.update({
                    "framework": "django",
                    "machine": platform.machine(),
                    "system": platform.system(),
                    "kernel": platform.release(),
                    "python": platform.python_version(),
                    "version": " ".join([str(p) for p in django.VERSION]),
                })

            #print("Scoreboard:", result)

            try:
                result = requests.post(url="{}/api/v1/metrics".format(self.api_url),
                                       json=result,
                                       headers={"project": self.project_key},
                                       timeout=3)


                self.first_contact = False

                for trace in self.tracer_manager:

                    print("Sending:", trace)

                    requests.post(url="{}/api/v1/trace".format(self.api_url),
                                  json=trace,
                                  headers={"project": self.project_key},
                                  timeout=3)

                    time.sleep(.1) # Avoid load peaks


            except requests.exceptions.ConnectionError:
                logging.warning("unable to connect to '{}', performance stats collection disabled".format(self.api_url))
                pass

            self.last_send = time.time()


            time.sleep(self.collector_interval)


    def stop(self):
        self.stop_requested = True


