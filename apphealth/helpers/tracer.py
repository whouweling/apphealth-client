
import time

import threading

from apphealth.setup import TRACERS
import logging

logger = logging.getLogger("Tracer")


class TracerManager(object):

    def __init__(self):
        self.last_trace = {}
        self.traces = {}

    def need_trace(self, view):
        return view not in self.last_trace

    def register_trace(self, view, name, trace):

        key = "{view}:{name}".format(view=view,
                                     name=name)

        self.last_trace[key] = time.time()
        self.traces[key] = {
            "view": view,
            "name": name,
            "trace": trace
        }

    def __next__(self):

        for key in list(self.traces.keys()):

            reg = self.traces[key]
            del self.traces[key]

            if reg:
                return reg

        raise StopIteration()

    def __iter__(self):
        return self


tracing_lock = threading.Lock()

class Tracer(object):

    def __init__(self):
        self.running = None
        self.tracers = []
        for tracer_class in TRACERS:
            self.tracers.append(tracer_class())

    def start(self):

        if not tracing_lock.acquire(False):
            return False

        logger.debug("Starting trace on thread {thread} ...".format(thread=threading.get_ident()))

        for tracer in self.tracers:
            tracer.install()

        self.running = True
        return True

    def stop(self):

        if not self.running:
            return

        for tracer in self.tracers:
            tracer.uninstall()

        self.running = False

        result = {}
        for tracer in self.tracers:
            result[tracer.get_name()] = tracer.flush()

        logger.debug("Completed trace on thread {thread}.".format(thread=threading.get_ident()))
        tracing_lock.release()

        return result

