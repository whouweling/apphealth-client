

class Sample:

    def __init__(self):
        self.values = {}
        self.max = {}
        self.min = {}
        self.count = 0

    def add(self, values):

        for key, value in values.items():

            if not key in self.values:
                self.values[key] = value
            else:
                self.values[key] += value

            if not key in self.max:
                self.max[key] = value
            else:
                if value > self.max[key]:
                    self.max[key] = value

            if not key in self.min:
                self.min[key] = value
            else:
                if value < self.min[key]:
                    self.min[key] = value

        self.count += 1

    def incr(self, key):
        if not key in self.values:
            self.values[key] = 1
        else:
            self.values[key] += 1

        self.count += 1


    def snapshot(self):
        result = {}

        for key, value in self.values.items():
            result[key] = round(value, 3)
            result["{}_max".format(key)] = round(self.max[key], 3)
            result["{}_min".format(key)] = round(self.min[key],3)

        self.values = {}
        self.count = 0
        return result


class SumSample(Sample):

    def snapshot(self):
        result = {}

        for key, value in self.values.items():
            result[key] = value

        self.values = {}
        self.count = 0
        return result