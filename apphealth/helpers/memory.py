#!/usr/bin/python
import re


def get_memory_usage():

    with open("/proc/meminfo", "r") as meminfo:
        parse = re.compile("^([a-zA-Z]+):[ ]+([0-9]+)[ ].*")

        stats = {}

        for line in meminfo:
            line = line[:-1]

            matches = parse.match(line)

            if matches != None:
                stats[matches.group(1)] = matches.group(2)

        meminfo.close()

        used_memory = int(((float(stats['MemTotal']) - float(stats['MemFree'])) - float(stats['Cached'])) / float(stats['MemTotal']) * 100)

        used_swap = None

        if int(stats['SwapTotal']) > 0:
            used_swap = int(float(int(stats['SwapTotal']) - int(stats['SwapFree'])) / float(stats['SwapTotal']) * 100)

        return {
            "used": used_memory,
            "swap": used_swap
        }
