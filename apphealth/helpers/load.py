
import re


def get_load():

    # Determine the number of CPU's:
    cpu_count = 0
    cpu_re = re.compile("^processor\s+:.*")

    for line in open("/proc/cpuinfo"):
        if cpu_re.match(line):
            cpu_count = cpu_count + 1

    try:

        # Determine the 5 minute load average:
        with open("/proc/loadavg", "r") as loadavg:
            (l1, l2, l3, d1) = loadavg.readline().split(" ", 3)
            loadavg.close()

        # Convert the load number to a weighted load:
        load = int(float(l2) / cpu_count * 100)

        # Send metric:
        return load

    except ValueError:
        pass


    return None