import time
from collections import defaultdict

from apphealth.helpers.sample import SumSample, Sample


class ScoreBoard:

    def __init__(self):
        self.reset()
        self.last_snapshot = {}
        self.sample_count = 0

    def reset(self):

        self.view_samples = defaultdict(lambda: {
            "request": Sample(),
            "method": SumSample(),
            "user": SumSample(),
            "response": SumSample()
        })

        self.start_time = time.time()
        self.sample_count = 0

    def register(self, view, **stats):

        metrics = ["duration",
                   "cpu",
                   "wait",
                   "response_size"]

        self.view_samples[view]["request"].add({
            metric: stats[metric] for metric in metrics
        })

        self.view_samples[view]["response"].incr(stats["response_code"])
        self.view_samples[view]["method"].incr(stats["request_method"])
        self.view_samples[view]["user"].incr(stats["user"])

        self.sample_count += 1

    def flush(self):

        result = {
            "start": self.start_time,
            "end": time.time(),
            "views": {},
        }

        for view, samples in self.view_samples.items():

            count = samples["request"].count

            if not count:
                continue

            view_result = {"count": count}

            for key, sample in samples.items():
                view_result[key] = sample.snapshot()

            #print("view_result:", view_result)
            result["views"][view] = view_result

        self.reset()

        return result