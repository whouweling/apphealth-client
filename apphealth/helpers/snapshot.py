import time

from apphealth.helpers.sample import SumSample, Sample


class Snapshot(object):

    def __init__(self):
        self.reset()

    def reset(self):
        self.snapshot = {}
        self.snapshot_collected = {}
        self.snapshot_time = {}

    def need_snapshot(self, view):
        return not view in self.snapshot_collected

    def register(self, view, snapshot):
        self.snapshot[view] = snapshot
        self.snapshot_collected[view] = time.time()

    def flush(self):
        result = self.snapshot
        self.reset()
        return result

    def __next__(self):

        if not len(self.snapshot):
            raise StopIteration()

        for item in self.snapshot.items():
            del self.snapshot[item[0]]
            return item

    def __iter__(self):
        return self


class QuerySnapshot(Snapshot):

    def register(self, view, queries):

        #print("QuerySnapshot: registered query snapshot for %s" % view)

        query_snapshot_count = {}
        query_snapshot_duration_max = {}

        for query in queries:

            (sql, duration) = query

            if not sql in query_snapshot_count:
                query_snapshot_count[sql] = 0

            if duration > query_snapshot_duration_max.get(sql, 0):
                query_snapshot_duration_max[sql] = duration

            query_snapshot_count[sql] += 1

        snapshot = {}
        for sql, count in query_snapshot_count.items():
            snapshot[sql] = {
                "count": count,
                "max_duration": query_snapshot_duration_max[sql] / 1000 / 1000 # Convert to seconds
            }

        return super(QuerySnapshot, self).register(view=view,
                                                   snapshot=snapshot)

